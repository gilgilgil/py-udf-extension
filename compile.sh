#!/bin/bash

#Import tools for compiling extension binaries
export PATH=$PATH:/usr/lib/ure/bin/
export PATH=$PATH:/usr/lib/libreoffice/sdk/bin/

#Setup directories 
mkdir "${PWD}"/PyUDF/
mkdir "${PWD}"/PyUDF/META-INF/

#Compile the binaries
idlc "${PWD}"/idl/XPyUDF.idl
regmerge -v "${PWD}"/PyUDF/XPyUDF.rdb UCR "${PWD}"/idl/XPyUDF.urd
rm "${PWD}"/idl/XPyUDF.urd

#Copy extension files and generate metadata
cp -f "${PWD}"/src/udf.py "${PWD}"/PyUDF/
cp -f "${PWD}"/src/description-en-US.txt "${PWD}"/PyUDF/
python "${PWD}"/src/generate_metainfo.py

#Package into oxt file
pushd "${PWD}"/PyUDF/
zip -r "${PWD}"/PyUDF.zip ./*
popd
mv "${PWD}"/PyUDF/PyUDF.zip "${PWD}"/PyUDF.oxt

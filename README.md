PyUDF Extension for LibreOffice Calc (Add-in)
===
The PyUDF extension is a template to quickly create a python User-Defined Function for LibreOffice Calc.

### Installation

1. Clone this project and run compile.sh
2. This will create a file named 'PyUDF.oxt'
3. In LibreOffice Calc, open the Extension Manager (Tools->Extention Manager... or Ctrl-Alt-E)
4. In the Extension Manager, Click 'Add' and browse to 'PyUDF.oxt'

### Usage

The PyUDF Extension adds a new function to Calc:  
```
USERDEFINEDFUNCTION(param1, param2)
```

You may type in any cell '=USERDEFINEDFUNCTION("hi", "there")'

The python code that is in 'udf.py' will be called and the fuction will run and return the string "hello"

Now you may modify the function, add new functions and re-compile and re-install the Add-in to make your python function crunch data from LibreOffice Calc

### License

This PyUDF Extension is released under the [![][shield:LGPL3]][License:3.0] which in layman's terms means:  

* You are permitted to use, copy and redistribute the work "as-is".
* You may adapt, remix, transform and build upon the material, releasing any derivatives under your own name.
* You may use the material for commercial purposes as long as the derivative is licenced under the GPL.
* You must track changes you make in the source files.
* You must include or make available the source code with your release.

### SMF Extension - thank you!

PyUDF is based on the SMF Extension written by [madsailor](https://github.com/madsailor) which can be viewed/downloaded here [![][shield:release-latest]][GIT:release]

The SMF extension can also be found on the [LibreOffice Extension Center](http://extensions.libreoffice.org/extension-center/smf-extension).

[GIT:release]: http://github.com/madsailor/SMF-Extension/releases/latest
[License:3.0]: http://www.gnu.org/licenses/lgpl.html
[shield:release-latest]: http://img.shields.io/github/release/madsailor/SMF-Extension.svg
[shield:LGPL3]: http://img.shields.io/badge/license-LGPL%20v.3-blue.svg

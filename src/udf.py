#  udf.py - Pyuno/LO bridge to implement new functions for LibreOffice Calc
#
#  Copyright (c) 2013 David Capron (drbluesman@yahoo.com)
#
#  license: GNU LGPL
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#

import unohelper
from org.pyudf import XPyUDF

class PyUDFImpl(unohelper.Base, XPyUDF):
    """Define the main class for the SMF extension """    
    def __init__(self, ctx):
        self.ctx = ctx
    
    def userDefinedFunction(self, a):
        """Run user-defined function. Mapped to PyUNO through the XPyUDF.rdb file"""
        return "hello"
    
    def UDFOptionalParam (self, a, b):
        return "hi"
    
    def UDFReturnArray (self, a):
        res = []
        res.append(tuple(["1", "2"]))
        res.append(tuple(["3", "4"]))
        return res
    
def createInstance( ctx ):
    return PyUDFImpl( ctx )

g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation( \
    createInstance,"org.pyudf.PyUDFImpl",
        ("com.sun.star.sheet.AddIn",),)